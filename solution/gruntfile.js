const webpackConfig = require("./webpack.config");

module.exports = function (grunt) {
  grunt.initConfig({
    connect: {
      server: {
        options: {
          hostname: "localhost",
          port: 5000,
          base: "",
          livereload: true,
        },
      },
    },

    cssmin: {
      target: {
        files: {
          "dist/css/style.css": ["dist/css/style.css"],
        },
      },
    },

    sass: {
      dist: {
        files: {
          "dist/css/style.css": "src/css/main.scss",
        },
      },
    },

    watch: {
      options: {
        spawn: false,
        livereload: true,
      },
      scripts: {
        files: ["*.html", "src/css/**/*.scss", "src/js/**/*.js"],
        tasks: ["sass", "cssmin", "webpack"],
      },
    },

    webpack: {
      options: {
        stats: !process.env.NODE_ENV || process.env.NODE_ENV === "development",
      },
      prod: webpackConfig,
      dev: webpackConfig,
    },
  });

  grunt.loadNpmTasks("grunt-contrib-connect");
  grunt.loadNpmTasks("grunt-contrib-cssmin");
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-webpack");

  grunt.registerTask("default", [
    "sass",
    "cssmin",
    "connect",
    "webpack",
    "watch",
  ]);
};
