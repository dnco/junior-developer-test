// import $ from "jquery";

// $(document).ready(function () {
//   // alert("hello, this is jquery working");
// });

import buildings from "../../buildings.json";

class App {
  selectedBuilding = null;

  renderBuilings = () => {
    if (buildings && buildings.length) {
      const elems = buildings.map((building) => `<div>${building.name}</div>`);
      const $buildingsList = document.querySelector(".buildings");

      buildings.forEach((building) => {
        $buildingsList.insertAdjacentHTML(
          "beforeEnd",
          `<li>
            <img src="./assets/${building.imageSrc}" alt="${building.name}" />
            <div class="copyBlock">
              <h2>${building.name}</h2>
              <p><span>Type:</span> ${building.type}</p>
              <p><span>Status:</span> ${building.status}</p>
              <p><span>Gross area:</span> ${building.grossArea
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")} sq ft</p>
              <button data-buildingId=${building.id}>
                View details
              </button>
            </div>
          </li>`
        );
      });
    }
  };

  renderSidebar = () => {
    // console.log(this.selectedBuilding);
    const sidebar = document.getElementById("sidebarContainer");
    const template = `
      <div>
        <img src="./assets/${this.selectedBuilding.imageSrc}" alt="${
      this.selectedBuilding.name
    }" />
        <div class="copyBlock">
          <h2>${this.selectedBuilding.name}</h2>
          <p class="meta"><span>Type:</span> ${this.selectedBuilding.type}</p>
          <p class="meta"><span>Location:</span> ${
            this.selectedBuilding.location
          }</p>
          <p class="meta"><span>Status:</span> ${
            this.selectedBuilding.status
          }</p>
          <p class="meta"><span>Gross area:</span> ${this.selectedBuilding.grossArea
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")} sq ft</p>
          <p class="meta"><span>Current total available space:</span> ${this.selectedBuilding.floors
            .reduce((a, b) => {
              console.log(a);
              console.log(a.availableSpace);
              return parseInt(a, 10) + parseInt(b.availableSpace, 10);
            }, 0)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")} sq ft</p>
          <p class="description">${this.selectedBuilding.description}</p>
          <div class="table">
            <div class="row header">
              <div>Floor</div>
              <div>Available Space</div>
              <div>Occupier(s)</div>
            </div>
            ${this.selectedBuilding.floors
              .map(
                (floor) => `
                <div class="row">
                  <div>${floor.label}</div>
                  <div>${
                    floor.disabled
                      ? "-"
                      : floor.availableSpace
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " sq ft"
                  }</div>
                  <div>${floor.occupier ? floor.occupier : ""}</div>
                </div>
              `
              )
              .join("")}
          </div>
        </div>
      </div>
    `;

    sidebar.innerHTML = template;
  };

  initBuildingButtons = () => {
    const $buttons = document.querySelectorAll(".buildings li button");

    $buttons.forEach(($button) => {
      $button.addEventListener("click", (event) => {
        this.selectedBuilding = buildings.find(
          (building) => building.id === event.target.dataset.buildingid
        );
        document.querySelector(".sidebar").classList.add("active");
        document.querySelector(".container").classList.add("sidebarActive");
        this.renderSidebar();
      });
    });
  };

  initSidebarButton = () => {
    const $closeBtn = document.querySelector(".sidebar button");

    $closeBtn.addEventListener("click", (e) => {
      e.preventDefault();
      document.querySelector(".sidebar").classList.remove("active");
      document.querySelector(".container").classList.remove("sidebarActive");
      window.setTimeout(() => {
        this.selectedBuilding = null;
      }, 600);
    });
  };

  init = () => {
    this.renderBuilings();
    this.initBuildingButtons();
    this.initSidebarButton();
  };
}

document.addEventListener("DOMContentLoaded", function () {
  const app = new App();

  app.init();
});
